//QUESTION 2 - INSERTING DOCUMENTS

db.movies.insert({
	title :"The Hobbit: An Unexpected Journey",
	writer:"J.R.R. Tolkein",
	year:2012,
franchise: "TheHobbit2"
})

function insertMovie(title, writer,year, actors, franchise, synopsis){
	db.movies.insert({
		title:title,
		writer: writer,
		year: year,
		actors:actors,
		franchise: franchise,
		synopsis: synopsis
	})
}

insertMovie("Fight Club", "Chuck Palahniuk", 1999,["Brad Pitt", "Edward Norton"])

insertMovie("Pulp Fiction", "Quentin Tarantino", 1994, ["John Travolta ","Uma Thurman "])

insertMovie("Inglorious Basterds", "Quentin Tarantino", 2009, ["Brad Pitt", "Diane Kruger", "Eli Roth"])

insertMovie("The Hobbit: An Unexpected Journey", "J.R.R. Tolkein", 2012, "", "The Hobbit")	

insertMovie("The Hobbit: The Desolation of Smaug", "J.R.R. Tolkein", 2013,"", "The Hobbit")

insertMovie("The Hobbit: The Battle of the Five Armies", "J.R.R. Tolkein", 2012,"","The Hobbit", "Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness")

insertMovie("Pee Wee Herman's Big Adventure")

insertMovie("Avatar")


//QUESTION 3 QUERY/ Find documents
//1) All documents
db.movies.find().pretty()

//2) All document with Quentin Tarantino as a writer
db.movies.find({writer:	("Quentin Tarantino")}).pretty()

//3) All dcooument where actors include Bradd Pitt
db.movies.find({actors :'Brad Pitt'}).pretty()

//4) All documents where the franchise is The hobbit
db.movies.find({franchise:	("The Hobbit")}).pretty()

//5) All movies released in the 90s
db.movies.find({year:{$gt:1989,$lt:2000}}).pretty()

//6) All movies released before 2000 or after 2010
db.movies.find( { $or: [ { year: { $lt: 2000 } }, { year: {$gt:2010} } ] } ).pretty()


//QUESTION 4 UPDATING
//1) adding a synopsis to The Hobbit: An Unexpected Journey 
db.movies.update(
{_id : ObjectId("5bdc4d85629c17ca48d47bf6")},
{$set: {synopsis: "A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug."}});
	
//2) adding a synopsis toThe Hobbit: The Desolation of Smaug 
db.movies.update(
{_id : ObjectId("5bdc4d85629c17ca48d47bf7")},
{$set: {synopsis: "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring"}});

//3) adding  Samuel L. Jackson to the actors in pulp fiction
db.movies.update(
{_id : ObjectId("5bdc49a6629c17ca48d47beb")},
{$push: {actors: "Samuel L. Jackson"}});
	


//QUESTION 5 TEXT SEARCH
//1) movies that have a synopsis contating the word "bilbo"
db.movies.find(
{synopsis: /Bilbo/},
{_id:0, title:1})
	
//2) movies that have a synopsis containing the word "Gandalf"
db.movies.find(
{synopsis: /Gandalf/},
{_id:0, title:1})

//3) movies that have a synopsis containing the words "Bilbo" and not Gandalf"
db.movies.find( 
{$and : [{synopsis: /Bilbo/},{synopsis: {$not :/Gandalf/}}]},
{_id:0, title:1, synopsis:1}).pretty();


//4) movies that have a synopsis containing the words "Dwarves" or "hobbit"
db.movies.find( 
{$or : [{synopsis: /dwarves/},{synopsis: /hobbit/}]},
{_id:0, title:1, synopsis:1}).pretty();

//4) movies that have a synopsis containing the words "gold" and "dragon"
db.movies.find( 
{$and : [{synopsis: /gold/},{synopsis: /dragon/}]},
{_id:0, title:1, synopsis:1}).pretty();


//QUESTION 6 DELETING
//1) delete movie Pee Wee Herman's Big Adventure
db.movies.remove({title:"Pee Wee Herman's Big Adventure"})

//1) delete movie Avatar
db.movies.remove({title:"Avatar"})


//QUESTION 7 relationships
db.users.insert({
	username:"GoodGuyGreg",
	first_name:"Good Guy",
	last_name:"Greg"
})

db.users.insert({
	username:"ScumbagSteve",
	full_name:[
	{first:"Scumbag", last:"Steve"}]
})





insertPost("GoodGuyGreg","Passes out at a party","wakes up early and cleans house")
insertPost("GoodGuyGreg","steals your identity","raises your credit score")
insertPost("GoodGuyGreg","reports a bug in your code","sends you a pull request")
insertPost("ScumbagSteve","borrows something", "sells it")
insertPost("ScumbagSteve","Borrows everything","the end")
insertPost("ScumbagSteve","Forks your repo on github","sets to private")

function insertComment(username, comment,post){
	db.comments.insert({
		username:username,
		commment: comment,
		post: post,
			})
}


insertComment("GoodGuyGreg","Hope you got a good deal!","5bdccf270e66dee656c1c7dc")

insertComment("GoodGuyGreg","What's mine is yours!","5bdccf270e66dee656c1c7dd")

insertComment("GoodGuyGreg","Don't violate the licensing agreement! ","5bdccf290e66dee656c1c7de")

insertComment("ScumbagSteve","It still isn't clean ","5bdccf270e66dee656c1c7d9")

insertComment("ScumbagSteve","Denied your PR cause I found a hack ","5bdccf270e66dee656c1c7db")


//QUESTION 8 querying related collections
//1) find all users
db.users.find().pretty()

//2) find all posts
db.posts.find().pretty()

//3) find all posts authored by "GoodGuyGreg"
db.posts.find({username :'GoodGuyGreg'}).pretty()

//4) find all posts authored by "ScumbagSteve"
db.posts.find({username :'ScumbagSteve'}).pretty()

//5) find all comments
db.comments.find().pretty()

//6) find all commments authored by "GoodGuyGreg"
db.comments.find({username:"GoodGuyGreg"}).pretty()

//7) find all commments authored by "ScumbagSteve"
db.comments.find({username:"ScumbagSteve"}).pretty()

//8) find all comments belonging to the post "Reports a bug in your code"
db.comments.find({post:"5bdccf270e66dee656c1c7db"}).pretty()




//POSTGRES 
CREATE TABLE writer(
writer_id SERIAL PRIMARY KEY,
name varchar(70)
);

CREATE TABLE movie(
movie_id SERIAL PRIMARY KEY,
title varchar(70),
year int,
franchise varchar(40),
synopsis varchar(255),
writer_id int REFERENCES writer(writer_id));

CREATE TABLE actors(
actor_id SERIAL PRIMARY KEY,
name varchar(40)
);

create table movie_actors(
movie_id int references movie(movie_id),
actor_id int references actors(actor_id)
);

--inserting writers
INSERT INTO writer(name) VALUES('Quentin Tarantino');
INSERT INTO writer(name) VALUES('Chuck Palahniuk');
INSERT INTO writer(name) VALUES('J.R.R. Tolkein');

--inserting actors
insert into actors(name) VALUES ('Brad Pitt'),('Edward Norton'),('John Travolta'),('Uma Thurman'),('Diane Kruger'),('Eli Roth');

--inserting movies
insert into movie (title,year,writer_id) VALUES('Fight Club',1999,2);
insert into movie (title,year,writer_id) VALUES('Pulp Fiction',1994,1);
insert into movie (title,year,writer_id) VALUES('Inglorious Basterds',2009,1);
insert into movie (title,year,writer_id, franchise) VALUES('The Hobbit: An Unexpected Journey',2012,3,'The Hobbit');
insert into movie (title,year,writer_id, franchise, synopsis) VALUES('The Hobbit: The Battle of the Five Armies ',2012,3,'The Hobbit','Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness');
insert into movie (title,year,writer_id, franchise, synopsis) VALUES('The Hobbit: The Desolation of Smaug',2013,3,'The Hobbit', 'Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness');
insert into movie(title) VALUES ('Pee Wee Hermans Big Adventure'),('Avatar');


--inserting movies actors
insert into movie_actors values(1,1),(1,2),(2,3),(2,4),(3,1),(3,5),(3,6);

insert into movie_actors values(1,12);


--Query/find documents
--1) gel all documents 

 SELECT m.*,a.name as actor,w.name as writer
  FROM movie m 
  full JOIN movie_actors ma on m.movie_id =ma.movie_id
  full JOIN actors a ON a.actor_id= ma.actor_id
  full join writer w on w.writer_id= m.writer_id;
  
  select* from movie;
  
  --2) get all documents with Quentin Tarantino as a writer 
select m.*, w.name as Writer 
from movie m , writer w
where m.writer_id=w.writer_id and w.name = 'Quentin Tarantino';

  
  --3) all documents where actors include bradd pitt 
  SELECT m.*,a.name,w.name 
  FROM movie m 
  JOIN movie_actors ma on m.movie_id =ma.movie_id
  JOIN actors a ON a.actor_id= ma.actor_id
  join writer w on w.writer_id= m.writer_id
  WHERE a.name = 'Brad Pitt';
  
  --4)all documents with franchise the hobbit
  SELECT m.*,a.name,w.name 
  FROM movie m 
  left JOIN movie_actors ma on m.movie_id =ma.movie_id
  left JOIN actors a ON a.actor_id= ma.actor_id
  left join writer w on w.writer_id= m.writer_id
   WHERE m.franchise LIKE '%The Hobbit%';

  --5)all documents released in the 90s
  SELECT m.*,a.name,w.name 
  FROM movie m 
  JOIN movie_actors ma on m.movie_id =ma.movie_id
  JOIN actors a ON a.actor_id= ma.actor_id
  join writer w on w.writer_id= m.writer_id
  WHERE m.year BETWEEN 1990 AND 2000;
  
  select * from movie
  Where year between 1990 and 2000;
  
 
  
  --6)
  SELECT m.*,a.name,w.name 
  FROM movie m 
  left JOIN movie_actors ma on m.movie_id =ma.movie_id
  left JOIN actors a ON a.actor_id= ma.actor_id
  left join writer w on w.writer_id= m.writer_id
  WHERE m.year >2010 OR m.year <2000;
  
  
  
 
  --4)UPDATE DOCUMENTS 
  --1) add synopsis to The Hobbit: An Unexpected Journey
  update movie
  set synopsis ='A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.' 
  where title ='The Hobbit: An Unexpected Journey';
  
 --2) add synopsis to The Hobbit:he Desolation of Smaug
  update movie
  set synopsis ='The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring' 
  where title ='The Hobbit: The Desolation of Smaug';
   
 --3) add an actor named  Samuel L. Jackson" to the movie "Pulp Fiction"
   
insert into actors(name) values('Samuel L. Jackson'); --adding the actor to the actors table 
insert into movie_actors( movie_id, actor_id)
VALUES((select movie_id from movie where title= 'Pulp Fiction'),(select actor_id from actors where name= 'Samuel L. Jackson'));

--5) Text search
--1) all movies that have a synopsis that contains the word bilbo
SELECT title from movie 
where synopsis  LIKE '%Bilbo%';

--2) all movies that have a synopsis that contains the word Gandalf
SELECT title from movie 
where synopsis  LIKE '%Gandalf%';


--3) all movies that have a synopsis that contains the word bilbo and not the word Gandalf
SELECT title from movie 
where synopsis LIKE '%Bilbo%' AND synopsis NOT LIKE '%Gandalf%';

--4)all movies that contains the word "dwarves" or "hobbit"
SELECT title from movie 
where synopsis LIKE '%dwarves%' OR synopsis  LIKE '%Gandalf%';

--5) all movies with a synopsis containting the word "gold" or dragon
SELECT title from movie 
where synopsis LIKE '%gold%' OR synopsis  LIKE '%dragon%';



--6)DELETING 
--1) delete the movie "Pee Wee Herman's Big Adventure"
delete from movie where title ='Pee Wee Hermans Big Adventure'

--1) delete the movie "Avatar"
delete from movie where title ='Avatar'



--7)relationships
CREATE TABLE users(
user_id SERIAL PRIMARY KEY,
username varchar(50),
name varchar(70));

insert into users (username, name) values ('GoodGuyGreg', 'Good Guy Greg'),('ScumbagSteve', 'Scumbag Steve')

create table posts(
post_id serial PRIMARY KEY,
title varchar(70),
body varchar(70),
user_id int references users(user_id));

insert into posts (title,body, user_id) values ('Passes out at party','Wakes up early and cleans house', 1), ('Steals your identity', 'Raises your credit score',1),('Reports a bug in your code','Sends you a Pull Request',1),('Borrows something','Sells it',2),('Borrows everything', 'The end',2),('Forks your repo on github','Sets to private',2);

create table comments(
comment_id serial primary key,
comment varchar(50),
post_id int references posts(post_id),
user_id int references users(user_id));

insert into comments(comment, post_id, user_id) 
values
('Hope you got a good deal!',(select post_id from posts where title='Borrows something'),1),
('Whats mine is yours!',(select post_id from posts where title='Borrows everything') ,1),
 ('Dont violate the licensing agreement!',(select post_id from posts where title='Forks your repo on github'),1), 
 ('It still isnt clean',(select post_id from posts where title='Passes out at party'),2), 
 ('Denied your PR cause I found a hack', (select post_id from posts where title='Reports a bug in your code'),2);
 
 
 --8) querying related documents
 --1) find all users
 select* from users;
 
  --2) find all posts
 select* from posts;

 
 --3) find all posts authored by "GoodGuyGreg"
 select posts.*, username
 FROM posts  
 join users
 on users.user_id= posts.user_id
 where username='GoodGuyGreg';
 
 
  --4) find all posts authored by "ScumbagSteve"
 select posts.*, username
 FROM posts  
 join users
 on users.user_id= posts.user_id
 where username='ScumbagSteve';

  --5) find all comments
 select* from comments;
 
 --6)find all comments authored by GoodGuyGreg
 select comments.*, username
 FROM comments  
 join users
 on users.user_id= comments.user_id
 where username='GoodGuyGreg';
 
 --7)find all comments authored by ScumbagSteve
 select comments.*, username
 FROM comments  
 join users
 on users.user_id= comments.user_id
 where username='ScumbagSteve';
 
  --7)find all comments belonging to the post "reports a bug in your code"
SELECT comments.*, posts.post_id, posts.title
from comments
join posts
on comments.post_id=posts.post_id
where posts.title='Reports a bug in your code';

	